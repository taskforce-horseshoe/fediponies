---
lang: pl
id: index
url: /
title: Fedi Ponies
contributors:
  - Polak23
---
## Czas zacząć!
### Czym są "Fedi Ponies"?
Fedi Ponies (Kucyki Fedi) to jest kolekcja instancji z tzw. Fediverse, głównie będące skierowane ku fanom My Little Pony i są przetłumaczone na różne języki dla wygody wszystkich użytkowników. Możesz wybrać swoją wymarzoną instancję Fedi Ponies jako swoją początkującą instancję i otworzyć bramę do ogromnej selekcji instancji z Fediverse!

### Co to jest "Fediverse"?
Fediverse (z ang. Federated Universe) to wzajemnie połączona sieć różnych oddzielnych sieci społecznościowych. Wszyscy użytkownicy Fediverse mają możliwość interacji ze sobą, bez jakichkolwiek trudności lub restrykcji. To bujny i organiczny zespół niezliczonych społeczności.

Fediverse to zróżnicowana platforma. Do mikroblogowania na wzór Twittera służą [Mastodon](https://joinmastodon.org/), [Misskey](https://misskey.page/), [Firefish](https://joinfirefish.org/) itp. [PixelFed](https://pixelfed.org/) do udostępniania zdjęć na wzór Instagrama, [PeerTube](https://joinpeertube.org/) do udostępniania wideo na wzór YouTube i wiele więcej.

Pomimo samych instancji, dużej ich liczby, Fediverse nie jest własnością jednego podmotu. W przeciwieństwie do korporacyjnych mediów społecznościowych, użytkownicy Fediverse zachowują własność swoich danych. Nie ma też żądzy zysku – instancje opierają się głównie na darowiznach, co sprawia, że piekło pełne reklam jest niezwykle mało prawdopodobne.

### Jak dołączyć do Fediverse?
To proste! Wybierz instancję, a następnie załóż na niej konto. Adresy e-mail są często wymagane, aby przejść proces weryfikacji, ale te e-maile częściej trafiają do folderu spamu, więc sprawdź również folder ze spamem. Gdy Twoje konto przekroczy próg wstępu, witaj w Fediverse!

### Czy muszę komunikować po angielsku?
W Fediverse zachęca się do używania języka angielskiego, ale nie jest to ani wymóg, ani obowiązek moralny. Możesz swobodnie dyktować, w jakim języku chcesz mówić, nawet po polsku!

### Co się stanie, jeśli uznam domyślny interfejs za niezadowalający?
W większości przypadków spróbuj zainstalować aplikację według własnego gustu! Pośród wielu doskonałych klientów Fediverse, jeden z pewnością będzie idealny dla ciebie, podczas przy użyciu przeglądarki można znaleźć także arkusze stylów i rozszerzenia dedykowane upiększaniu i kustomizacji istniejących interfejsów.

## Następne kroki
A teraz na co czekasz? [Wybierz serwer i dołącz do Fediverse](./next/)!
