---
lang: es
id: index
url: /
title: Ponis del Fediverso
contributors:
  - Sweet Dreams
---
## ¡Vamos!

### ¿Qué son los Ponis del Fediverso?

**Los Ponis del Fediverso** es una colección de instancias del Fediverso, principalmente orientadas hacia el fandom de My Little Pony, y localizadas a múltiples lenguajes para la facilidad de uso de todo el mundo. Con los **Ponis del Fediverso**, elige la instancia de tus sueños como comunidad en la que empezar, y ¡abre las puertas hacia un Fediverso más grande!

### ¿Qué es el Fediverso?

El Fediverso (universo federado) es una red interconectada de distintas redes sociales. Todos los miembros del fediverso pueden interactuar con otros si no hay restricciones adicionales. Es un conjunto fértil y orgánico de comunidades sin fin.

El fediverso es una plataforma diversa. Para microblogging al estilo de Twitter, están [Mastodon](https://joinmastodon.org), [Misskey](https://misskey.page), [Firefish](https://joinfirefish.org) y otros; [PixelFed](https://pixelfed.org) está para compartir imágenes al estilo de Instagram; [PeerTube](https://joinpeertube.org) para compartir vídeos al estilo de YouTube, y muchos más.

Instancias individuales aparte, el Fediverso no es propiedad de una única entidad, con lo que no puede ser controlado por una sola entidad. Al contrario que las redes sociales controladas por corporaciones, los usuarios del Fediverso mantienen su control sobre sus propios datos. Además, el Fediverso no tiene motivaciones lucrativas: las instancias dependen de donaciones y no están infestadas de anuncios.

### ¿Cómo puedo unirme al Fediverso?

¡Es sencillo! Elige una instancia y regístrate en ella. Por lo general se requiere una dirección de correo electrónico para el proceso de verificación, aunque esos correos con frecuencia entran en la carpeta de spam, así que asegúrate de comprobar tu filtro de spam también. Una vez tu cuenta es admitida, ¡puedes unirte a la fiesta!

### ¿Necesito saber inglés?

El uso de inglés es común en el Fediverso, pero no es un requisito. Eres libre de usar el lenguaje que prefieras.

### ¿Que pasa si la interfaz por defecto no me satisface?

¡Puedes instalar una aplicación a tu propio gusto! Hay varios clientes excelentes para el Fediverso, y definitivamente alguno te gustará. Si usas un explorador, también hay hojas de estilo y extensiones dedicadas a dar estética a las interfaces existentes.

## Siguientes pasos

¿A qué esperas? [Elige un servidor y únete al Fediverso](./next/)!
