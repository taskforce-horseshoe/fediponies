---
lang: de
id: index
url: /
title: Fedi Ponies
contributors:
  - interru
---
## Auf gehts!
### Was ist Fedi Ponies?

**Fedi Ponies** ist eine Sammlung von Fediverse-Instanzen, hauptsächlich gerichtet an das My Little Pony Fandom, die für die Benutzerfreundlichkeit in vielen Sprachen übersetzt wurde. Such dir mit **Fedi Ponies** deine Traum-Instanz als Startpunkt für den Einstieg in das weite Fediverse!

### Was ist das Fediverse?
Das Fediverse (föderierte Universum) besteht aus einem Verbund verschiedener sozialer Netzwerke. Jeder im Fediverse kann mit jedem interagieren, sofern keine weiteren Einschränkungen bestehen. Es ist ein umfangreiches und organisches Ensemble von unzähligen Gemeinschaften.

Das Fediverse ist eine diverse Plattform. Für Twitter ähnliches Microblogging gibt es [Mastodon](https://joinmastodon.org), [Misskey](https://misskey.page), [Firefish](https://joinfirefish.org) usw. Bilder teilen, wie bei Instagram geht mittels [PixelFed](https://pixelfed.org), [PeerTube](https://joinpeertube.org) für YouTube-ähnliches Video teilen, und so vieles mehr.

Abgesehen von einzelnen Instanzen selbst ist im Großen und Ganzen das Fediverse nicht im Besitz von einem einzelnen Unternehmen. Es besteht nicht die Gefahr, dass ein Unternehmen die komplette Kontrolle über das Fediverse erhält. Anders als bei Unternehmens-geführten sozialen Medien behalten im Fediverse die Nutzer die Kontrolle über ihre Daten. Das Fediverse für sich ist auch nicht gewinnorientiert: Die meisten Instanzen werden mit Spenden betrieben. Es ist daher sehr unwahrscheinlich, auf eine werbe-verseuchte Hölle zu stoßen.

### Wie trete ich dem Fediverse bei?
Das ist ganz einfach! Such dir eine Instanz aus und erstell dir dort einen Account. Für die Verifikation wird häufig eine E-Mail-Adresse benötigt. Achte darauf, dass die E-Mails für die Verifikation häufig im Spam-Ordner landen. Sobald dein Account zugelassen wurde, kannst du loslegen: Willkommen im Fediverse!

### Muss ich Englisch sprechen?
Die Nutzung von Englisch wird im Fediverse empfohlen, aber es wird weder vorausgesetzt noch bist du moralisch dazu verpflichtet. Es ist deine freie Entscheidung, in welcher Sprache du deine Posts verfasst.

### Was, wenn ich mit der Standard-Benutzeroberfläche unzufrieden bin?
In der Regel hilft es, eine App zu suchen, die deinem Geschmack entspricht. Bei den verschiedenen Fediverse-Clients findest du bestimmt etwas für dich. Für den Browser gibt es zudem Stilvorlagen und Erweiterungen, um die Benutzeroberfläche zu verbessern.

## Nächste Schritte
Nun, worauf wartest du? [Such dir einen Server aus und trete dem Fediverse bei](./next/)!
